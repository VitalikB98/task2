<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.03.17
 * Time: 14:30
 */
namespace Drupal\amazing_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;




class RecordController extends ControllerBase {

    public function getById($id = NULL) {
        $query = \Drupal::database()->select('amazing_form','n');
        $query->fields('n', array('id', 'first_name', 'second_name', 'email'));

        if ($id) {
            $query->condition('id', $id);
        }

        $result = $query->execute()->fetchAll();

        return $result;
    }

    public function getOne($amazing_form_id = NULL) {
        // Output one record
        $output = array();

        $output['#title'] = 'One record';
        $result = $this->getById($amazing_form_id);
        $output['#markup'] = $result[0]->first_name . ' ' . $result[0]->second_name;

        return $output;
    }

    public function getAll() {
        // Output all records
        $output = array();

        $output['#title'] = 'All record';
        $result = $this->getById();

        if ($result) {
          $form['result'] = array(
              '#type' => 'table',
              '#header' => array($this->t('id'), $this->t('first_name'), $this->t('second_name'), $this->t('email'), ),
          );
          foreach ($result as $item) {
              $url = Url::fromRoute('amazing_form.one_record', array(
                  'amazing_form_id' => $item->id,
              ));
              $form['result'][] = array(
                  'id' => array(
                      '#type' => 'markup',
                      '#markup' => $item->id,
                  ),
                  'first_name' => array(
                      '#type' => 'markup',
                      '#markup' => \Drupal::l($item->first_name, $url),
                  ),
                  'second_name' => array(
                      '#type' => 'markup',
                      '#markup' => $item->second_name,
                  ),
                  'email' => array(
                      '#type' => 'markup',
                      '#markup' => $item->email,
                  ),
              );
          }
          return $form;
        }
        return FALSE;

    }

}

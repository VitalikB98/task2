<?php
/**
 * Created by PhpStorm.
 * User: fencer
 * Date: 10.03.17
 * Time: 11:00
 */
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */
namespace Drupal\amazing_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class ContributeForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'amazing_forms_contribute_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['first_name'] = array(
            '#type' => 'textfield',
            '#title' => t('First name'),
            '#required' => TRUE,
        );

        $form['second_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Second name'),
            '#required' => TRUE,
        );

        $form['email'] = array(
            '#type' => 'email',
            '#title' => t('Email'),
        );

        $form['phone_number'] = array(
            '#type' => 'tel',
            '#title' => t('Phone number'),
        );

        $form['birth_day'] = array(
            '#type' => 'date',
            '#title' => t('Birth day'),
            '#required' => TRUE,
        );

        $form['develop'] = array(
            '#type' => 'checkbox',
            '#title' => t('I would like to be involved in developing'),
        );

        $form['about_you'] = array(
            '#type' => 'textarea',
            '#title' => t('About you'),
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        // check phone number
        $number_format = !preg_match("/^[0-9]{3,12}+$/" ,$form_state->getValue('phone_number'));
        $not_empty = !empty( trim($form_state->getValue('phone_number')) );

        if ( $number_format && $not_empty ) {
            $form_state->setErrorByName(
                'phone_number',
                $this->t('The phone number has wrong format.')
            );
        }

        // check date
        if ( strtotime($form_state->getValue('birth_day')) >= time() ) {
            $form_state->setErrorByName(
                'birth_day',
                $this->t('The date has wrong format.')
            );
        }


    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        // Insert information into "amazing_form" table
        $data = db_insert('amazing_form')->fields(
            array(
                'first_name' => $form_state->getValue('first_name'),
                'second_name' => $form_state->getValue('second_name'),
                'email' => $form_state->getValue('email'),
                'phone_number' => $form_state->getValue('phone_number'),
                'birth_day' => $form_state->getValue('birth_day'),
                'develop' => $form_state->getValue('develop'),
                'changed' => time(),
                'about_you' => $form_state->getValue('about_you'),
            )
        )->execute();

        if ($data) {
            drupal_set_message(t('The information is saved'),'status');
        } else {
            drupal_set_message(t('Something wrong =('), 'error');
        }
    }
}
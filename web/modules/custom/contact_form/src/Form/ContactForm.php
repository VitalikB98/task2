<?php
/**
 * Created by PhpStorm.
 * User: fencer
 * Date: 14.03.17
 * Time: 16:33
 */
/**
 * @file
 * Contains: Drupal\contact_form\Form\ContactForm
 */
namespace Drupal\contact_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ContactForm extends FormBase {

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getFormId() {
        // TODO: Implement getFormId() method.
        return 'contact_form_custom';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['full_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Full name'),
            '#required' => TRUE,
        );
        //email
        $form['email'] = array(
            '#type' => 'email',
            '#title' => t('Email'),
            '#required' => TRUE,
        );
        //phone number
        $form['phone_number'] = array(
            '#type' => 'tel',
            '#title' => t('Phone'),
            '#required' => TRUE,
        );
        //subject
        $form['subject'] = array(
            '#type' => 'textfield',
            '#title' => t('Subject'),
            '#required' => TRUE,
        );
        //message
        $form['message'] = array(
            '#type' => 'textarea',
            '#title' => t('Message'),
            '#required' => TRUE,
        );
        //submit
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        // check phone number
        $number_format = !preg_match("/^[0-9]{3,15}+$/" ,$form_state->getValue('phone_number'));
        $not_empty = !empty( trim($form_state->getValue('phone_number')) );

        if ( $number_format && $not_empty ) {
            $form_state->setErrorByName(
                'phone_number',
                $this->t('The phone number has wrong format.')
            );
        }

        if ( )
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // TODO: Implement submitForm() method.
    }
}
